<?php
function k(int $i): string
{
    $star = "";
    for ($k = 1; $k <= (2 * $i - 1); ++$k) :
        $star .= ($k == 1 || $k == (2 * $i - 1)) ? "*" : "-";
    endfor;

    return $star;
}

function j(int $i, int $_nb): string
{
    $tiret = "";
    for ($j = $i; $j < $_nb * 2; ++$j) :
        $tiret .= "-";
    endfor;
    return $tiret;
}

function headStar(int $_nb): string
{
    $head = "";
    for ($i = 1; $i <= $_nb; ++$i) :
        $head .= container($i, $_nb);
    endfor;

    return $head;
}

function endStar(int $_nb): string
{
    $end = "";
    for ($i = $_nb; $i >= 1; --$i) :
        $end .= container($i, $_nb);
    endfor;

    return $end;
}

function container(int $i = 0, int $_nb = 0): string
{
    $container = "";
    $container .= j($i, $_nb);
    $container .= k($i);
    $container .= "<br />";

    return $container;
}

function star(int $_nb): void
{
    echo headStar($_nb) . endStar($_nb);
}

star(20);
